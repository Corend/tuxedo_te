#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

#Clean annot comparegff
gtable_file = sys.argv[1]
otable_file = sys.argv[2]

gtable = open(gtable_file,  'r')
otable = open(otable_file,'w')

for genes in gtable:
    gene = genes.rstrip().split("\t")
    if len(gene) <= 5: # If 5 or less fields, keep because no duplicated genes id
        otable.write(genes)
    else:
        fix_id = genes.rstrip().split(',')[0] # fix id is the part to keep for all duplicates
        supplementary = genes.rstrip().split(',')[1:]
        otable.write("\t".join(fix_id.split("\t")[0:5])+"\n")
        for supp in supplementary :
            otable.write("\t".join(fix_id.split("\t")[0:3])+"\t"+supp+"\n")

otable.close()
gtable.close()
