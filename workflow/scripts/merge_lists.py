#-*- coding: utf-8 -*-

# Input files
cds_list = snakemake.input["cds"]
note_list = snakemake.input["note"]
expr_list = snakemake.input["expr"]

cds = open(cds_list,"r")
note = open(note_list,"r")
expr = open(expr_list,"r")

t_cds = cds.readlines()
t_note = note.readlines()
t_expr = expr.readlines()

cds.close()
note.close()
expr.close()

inter = list (set(t_cds) & set(t_note) & set(t_expr))

print(len(inter))

inter_file = snakemake.output["gene"]
inter_t_file = snakemake.output["transcr"]

inter_f = open(inter_file,"w")
inter_t_f = open(inter_t_file,"w")

for trans in inter:
    inter_t_f.write(trans)
    inter_f.write(".".join(trans.split(".")[0:2]) + "\n")
inter_t_f.close()
inter_f.close()
