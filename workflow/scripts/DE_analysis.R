#DESeq2 for gene expression analysis

library(DESeq2,quietly = TRUE)
library(ggplot2,quietly=TRUE)
library(apeglm,quietly=TRUE)

gene_count_matrix = snakemake@input[["gene"]]

rowsofmatrix="gene_id"
Gene_matrix <- read.table(gene_count_matrix,h=T,sep=",",row.names=rowsofmatrix)
#Condition table :
Cond <- c(rep(snakemake@params[["control"]],snakemake@params[["nrep"]]),rep(snakemake@params[["treatment"]],snakemake@params[["nrep"]]))
Cond <- as.data.frame(Cond)
colnames(Cond) <- c("condition")

rownames(Cond) <- colnames(Gene_matrix)
RowCondNoOrder <- rownames(Cond)
Cond <- data.frame(condition=Cond[order(rownames(Cond)),])
rownames(Cond) <- RowCondNoOrder[order(RowCondNoOrder)]
Gene_matrix <- Gene_matrix[,order(colnames(Gene_matrix))]
# Convert in DESeq2:

colnames(Gene_matrix) <- rownames(Cond)
DEGene <- DESeqDataSetFromMatrix(countData = Gene_matrix,
                                 colData = Cond,
                                 design = ~ condition)
featureData <- data.frame(gene=rownames(Gene_matrix))
mcols(DEGene) <- DataFrame(mcols(DEGene), featureData)

#Enleve les gènes avec 1 ou 0 read :
DEGene <- DEGene[rowSums(counts(DEGene)) > 1,] #### Quels gènes exclure ?? Usefull for FDR

#Conditions :
DEGene$condition <- relevel(DEGene$condition, ref="Ovary")
DEGene <- DESeq(DEGene)

resLFC <- lfcShrink(DEGene, coef=2, type="apeglm",lfcThreshold = 1)

#Tri :
ResGeneOrdered <- resLFC[order(resLFC$svalue,-abs(resLFC$log2FoldChange)),]
ResDePrior <- as.data.frame(ResGeneOrdered[,c("log2FoldChange","svalue")])
ResDeBaseMean <- as.data.frame(ResGeneOrdered[,c("log2FoldChange","svalue","baseMean")])

ResMaplot <- transform(ResDeBaseMean, Organ= ifelse(log2FoldChange>=1 & svalue <0.005, "Testis", ifelse(log2FoldChange<=-1 & svalue <0.005, "Ovary","Not DE")))
maplot <- ggplot(as.data.frame(ResMaplot),aes(x=log10(baseMean),y=log2FoldChange,color=Organ))+
  geom_point(mapping = aes(size=Organ,alpha=Organ,shape=Organ,fill=Organ))+
  theme_bw()+
  theme(legend.position = 'none')+
  scale_size_manual(values = c(0.75,0.1,0.75))+scale_alpha_manual(values = c(0.8,1,0.8))+
  scale_shape_manual(values=c(21,21,21))+scale_fill_manual(values = c("#05100e","#999999","#05100e"))+
  scale_color_manual(values=c("#923731","#999999","#2e2c84"))

ggsave(snakemake@output[["maplot"]], plot = maplot, width = 14, height = 8,device = "pdf")
write.table(x=ResMaplot,file=snakemake@output[["de"]],append=F,quote=F,sep="\t")

# CEN genes part
CEN_gene_list = snakemake@input[["cen"]]
cen <- read.table(CEN_gene_list,h=F,sep="\t")$V1
cenMaplot <- ResMaplot[rownames(ResMaplot)%in%cen,]
write.table(x=cenMaplot,file=snakemake@output[["de_cen"]],append=F,quote=F,sep="\t")

maplot_cen <- ggplot(as.data.frame(cenMaplot),aes(x=log10(baseMean),y=log2FoldChange,color=Organ))+
  geom_point(mapping = aes(size=Organ,alpha=Organ,shape=Organ,fill=Organ))+
  theme_bw()+
  theme(legend.position = 'none')+
  scale_size_manual(values = c(0.75,0.1,0.75))+scale_alpha_manual(values = c(0.8,1,0.8))+
  scale_shape_manual(values=c(21,21,21))+scale_fill_manual(values = c("#05100e","#999999","#05100e"))+
  scale_color_manual(values=c("#923731","#999999","#2e2c84"))
ggsave(snakemake@output[["maplot_cen"]], plot = maplot_cen, width = 14, height = 8,device = "pdf")
