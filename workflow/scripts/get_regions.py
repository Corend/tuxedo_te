#-*- coding: utf-8 -*-

# Input file
gff3_file = snakemake.input["gff"]
gff3 = open(gff3_file,'r')
list_tr_file = snakemake.input["list_tr"]
list_tr_input = open(list_tr_file,'r')
list_ge_file = snakemake.input["list_ge"]
list_ge_input = open(list_ge_file,'r')

# Output files
cds_file = snakemake.output["cds"]
three_file = snakemake.output["utrt"]
five_file = snakemake.output["utrf"]
intron1_file = snakemake.output["introno"]
intron2_file = snakemake.output["intront"]
intron_file = snakemake.output["introns"]

cds = open(cds_file,"w")
three_prime = open(three_file,"w")
five_prime = open(five_file,"w")
intron1 = open(intron1_file,"w")
intron2 = open(intron2_file,"w")
intron = open(intron_file,"w")

# List Coding + expressed + not a TE
list_CEN = list_tr_input.read().splitlines()
list_CENge = list_ge_input.read().splitlines()

#Exonic regions
exons = ["CDS","three_prime_UTR","five_prime_UTR"]

# Upstream files
upstream_sizes = [int(x) for x in snakemake.params["size"]]
upstreams = {}
for size in upstream_sizes:
    upstreams[size] = open("regions/upstream" + str(size) + ".bed","w")

# Around files:
around_sizes = [int(x) for x in snakemake.params["size"]]
arounds = {}
for size in around_sizes:
    arounds[size] = open("regions/around" + str(size) + ".bed","w")

# Dict for introns
introns={}

for line in gff3:
    if line[0] != "#":
        features = line.split("\t")
        chrom = features[0]
        type = features[2]
        start = int(features[3])
        end = int(features[4])
        strand = features[6]

        if type in exons :
            transcript = ".".join(features[8].split(";")[1].split("=")[1].split(".")[0:3])
            gene = ".".join(features[8].split(";")[1].split("=")[1].split(".")[0:2])

        # Write CDS:
        if type == "CDS" and transcript in list_CEN:
            cds_out = chrom + "\t" + str(start-1) + "\t" + str(end) + "\t" + gene + "\n"
            cds.write(cds_out)

        # Write 3utr
        if type == "three_prime_UTR" and transcript in list_CEN:
            three_prime_out = chrom + "\t" + str(start-1) + "\t" + str(end) + "\t" + gene + "\n"
            three_prime.write(three_prime_out)

        # Write 5utr
        if type == "five_prime_UTR" and transcript in list_CEN:
            five_prime_out = chrom + "\t" + str(start-1) + "\t" + str(end) + "\t" + gene + "\n"
            five_prime.write(five_prime_out)

        # Write upstreams
        if type == "gene":
            for size in upstream_sizes:
                geneUP = features[8].split(";")[0].split("=")[1]
                if geneUP in list_CENge:
                    if strand == "+":
                        out = chrom + "\t" + str(int(max(start-size-1,0))) + "\t" + str(start) + "\t" + geneUP + "\n"
                        upstreams[size].write(out)
                        if strand == "-" :
                            out = chrom + "\t" + str(end) + "\t" + str(end+size) + "\t" + geneUP + "\n"
                            upstreams[size].write(out)

        # Write arounds
        if type == "gene":
            for size in around_sizes:
                geneDOWN = features[8].split(";")[0].split("=")[1]
                if geneDOWN in list_CENge:
                    if strand == "+":
                        out = chrom + "\t" + str(int(max(start-size/2,0))) + "\t" + str(int(max(start+size/2,0))) + "\t" + geneDOWN + "\n"
                        arounds[size].write(out)
                        if strand == "-" :
                            out = chrom + "\t" + str(int(max(end-size/2,0))) + "\t" + str(int(max(end+size/2,0))) + "\t" + geneDOWN + "\n"
                            arounds[size].write(out)

        if type == "intron":
            transcr = ".".join(features[8].rstrip().split("=")[1].split(".")[0:3])
            if transcr in list_CEN:
                if transcr not in introns:
                    nbint = 1
                    introns[transcr] = {}
                    introns[transcr]["strand"] = strand
                    introns[transcr]["chrom"] = chrom
                    introns[transcr]["introns"] = []
                introns[transcr]["introns"].append({"start":start-1, "end":end,"nbint":nbint})
                nbint += 1

# print intron files
for transcr in introns :
    for i in introns[transcr]["introns"]:
        intron.write(introns[transcr]["chrom"] + "\t" + str(i["start"]) + "\t" + str(i["end"]) + "\t" + ".".join(transcr.split(".")[0:2]) + "\n")

    if introns[transcr]["strand"] == "+":
        first_int =  introns[transcr]["introns"][0]
    if introns[transcr]["strand"] == "-":
        first_int =  introns[transcr]["introns"][-1]

    intron1.write(introns[transcr]["chrom"] + "\t" + str(first_int["start"]) + "\t" + str(first_int["end"]) + "\t" + ".".join(transcr.split(".")[0:2]) + "\n")

    for i in introns[transcr]["introns"]:
        #print(i)
        if introns[transcr]["strand"] == "+" and i["nbint"] != 1 or introns[transcr]["strand"] == "-" and i != introns[transcr]["introns"][-1]:
            intron2.write(introns[transcr]["chrom"] + "\t" + str(i["start"]) + "\t" + str(i["end"]) + "\t" + ".".join(transcr.split(".")[0:2]) + "\n")

# File close
cds.close()
three_prime.close()
five_prime.close()
intron2.close()
intron1.close()
intron.close()

for file in arounds:
    arounds[file].close()
for file in upstreams:
    upstreams[file].close()

gff3.close()
